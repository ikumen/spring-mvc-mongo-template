package com.gnoht.app.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CustomRepository<T, ID extends Serializable> 
		extends CrudRepository<T, ID>{

	public T findOneByName(String name);
}
