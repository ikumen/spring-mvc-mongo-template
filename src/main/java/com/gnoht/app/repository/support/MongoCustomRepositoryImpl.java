package com.gnoht.app.repository.support;

import java.io.Serializable;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;

import com.gnoht.app.repository.CustomRepository;

public class MongoCustomRepositoryImpl<T, ID extends Serializable> 
		extends SimpleMongoRepository<T, ID>
	implements CustomRepository<T, ID> {

	private MongoOperations mongoOperations;
	
	public MongoCustomRepositoryImpl(MongoEntityInformation<T, ID> metadata,
			MongoOperations mongoOperations) {
		super(metadata, mongoOperations);
		this.mongoOperations = mongoOperations;
	}

	@Override
	public T findOneByName(String name) {
		return mongoOperations.findOne(
				new Query(Criteria.where("name").is(name)), 
			getEntityInformation().getJavaType()
		);
	}
}
