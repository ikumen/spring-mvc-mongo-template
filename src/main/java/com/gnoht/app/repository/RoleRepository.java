package com.gnoht.app.repository;

import com.gnoht.app.model.Role;

public interface RoleRepository 
		extends CustomRepository<Role, String>{
}
