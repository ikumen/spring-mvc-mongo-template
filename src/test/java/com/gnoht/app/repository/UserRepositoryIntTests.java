package com.gnoht.app.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoDataIntegrityViolationException;

import com.gnoht.app.AbstractRepositoryIntTests;
import com.gnoht.app.model.Role;
import com.gnoht.app.model.User;
import com.gnoht.app.repository.UserRepository;

public class UserRepositoryIntTests
		extends AbstractRepositoryIntTests<UserRepository>{

	@Resource
	private UserRepository userRepository;

	@Resource
	private RoleRepository roleRepository;

	@Test
	public void testSave() {
		User user = buildUser("user");
		
		assertNull(user.getId());
		assertNotNull(userRepository.save(user).getId());
	}
	
	@Test
	public void testFindOneByName() {
		//given
		User user = buildUser("user");
		assertNull(userRepository.findOneByName(user.getName()));
		
		//when
		userRepository.save(user);

		//then
		assertNotNull(userRepository.findOneByName(user.getName()));
		assertNull(userRepository.findOneByName("sdkfasdfsdf"));
	}
	
	@Test(expected=MongoDataIntegrityViolationException.class)
	public void testUniqueEmailConstraints() {
		//given
		User user1 = buildUser("user1");
		userRepository.save(user1);
		
		//when
		User user2 = buildUser("user2");
		user2.setEmail(user1.getEmail());
		userRepository.save(user2);
		
		//then expect MongoDataIntegrityViolationException
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testEmailNotNullConstraint() {
		//given
		User user = buildUser("user");
		user.setEmail(null);
		userRepository.save(user);
	}
	
	@Test(expected=MongoDataIntegrityViolationException.class)
	public void testUniqueNameConstraint() {
		//given 2 users with name "user1"
		//when they are saved
		//then expect MongoDataIntegrityViolationException
		userRepository.save(buildUser("user1"));
		userRepository.save(buildUser("user1"));
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testNameMaxSizeConstraint() {
		//given name with 51 chars
		User user = buildUser("123456789012345678901234567890123456789012345678901");
		//when saved
		userRepository.save(user);

		//then expect ConstraintViolationException
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testNameMinSizeConstraint() {
		//given name with 1 chars
		User user = buildUser("1");
		//when saved
		userRepository.save(user);

		//then expect ConstraintViolationException
	}
	
	@Test
	public void testUserRole() {
		//given
		User user = buildUser("user");
		Role role = roleRepository.save(new Role("admin"));
		
		//when
		user.addRole(role);
		userRepository.save(user);
		
		//then
		List<User> foundUsersByRole = userRepository.findAllByRoles(role);
		assertEquals(1, foundUsersByRole.size());
		assertEquals(foundUsersByRole.get(0).getId(), user.getId());
	}
	
	User buildUser(String name) {
		User user = new User();
		user.setName(name);
		user.setEmail(name + "@domain.com");
		return user;
	}
	
	@Before
	public void setUp() {
		cleanUpRepository(roleRepository);
	}
	
	@After
	public void tearDown() {
		cleanUpRepository(roleRepository);
	}
	
	@Override
	public UserRepository getRepository() {
		return userRepository;
	}
}
