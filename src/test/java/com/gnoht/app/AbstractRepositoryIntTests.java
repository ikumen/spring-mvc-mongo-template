package com.gnoht.app;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

import com.gnoht.app.repository.CustomRepository;

/**
 * Abstract class providing base functionality for testing 
 * {@link CrudRepository} implementations. 
 */
@SuppressWarnings("rawtypes")
public abstract class AbstractRepositoryIntTests<T extends CustomRepository> 
		extends AbstractIntegrationTests {

	protected static final Logger LOGGER = 
		LoggerFactory.getLogger(AbstractRepositoryIntTests.class);
	
	public abstract T getRepository();
	
	@Before
	public void beforeSetUp() {
		cleanUpRepository(getRepository());
	}
	
	@After
	public void beforeTearDown() {
		cleanUpRepository(getRepository());
	}
	
	/**
	 * Cleans up {@link CustomRepository}. Called before/after each test.
	 * @param repository
	 */
	protected <R extends CustomRepository> 
			void cleanUpRepository(R repository) {
		LOGGER.info("Cleaning up repository: {}", repository.getClass().getSimpleName());
		repository.deleteAll();
		assertTrue("Unable to clean repository: " + repository.getClass(), 
				repository.count() == 0);
	}
}
